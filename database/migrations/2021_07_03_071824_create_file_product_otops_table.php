<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileProductOtopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_product_otops', function (Blueprint $table) {
            $table->increments('id');
            $table->text('filename');
            $table->integer('product_otop_id')->unsigned();
            $table->foreign('product_otop_id')
                ->references('id')
                ->on('product_otops')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_product_otops');
    }
}
