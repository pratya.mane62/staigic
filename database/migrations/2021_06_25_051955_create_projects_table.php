<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->date('project_start')->nullable();
            $table->date('project_end')->nullable();
            $table->integer('stargic_id')->unsigned();
            $table->integer('budget_year_id')->unsigned();
            $table->text('link')->nullable();
            $table->enum('type', ['project', 'side'])->default('project');
            $table->enum('level', [1, 2, 3])->default(1);
            $table->integer('parent_id')->unsigned()->nullable();

            $table->foreign('parent_id')
                ->references('id')
                ->on('projects')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('budget_year_id')
                ->references('id')
                ->on('budget_years')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('stargic_id')
                ->references('id')
                ->on('stargics')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
