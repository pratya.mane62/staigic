<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBudgetYearStargicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_year_stargic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_year_id')->unsigned();
            $table->integer('stargic_id')->unsigned();

            $table->foreign('stargic_id')
                ->references('id')
                ->on('stargics')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('budget_year_id')
                ->references('id')
                ->on('budget_years')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_year_stargic');
    }
}
