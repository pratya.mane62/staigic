require("./bootstrap");

// Import modules...
import { createApp, h } from "vue";
import { createInertiaApp } from "@inertiajs/inertia-vue3";
import { InertiaProgress } from "@inertiajs/progress";
import ElementPlus from "element-plus";
import "element-plus/lib/theme-chalk/index.css";
import VueGoogleMaps from "@fawmi/vue-google-maps";

import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

const el = document.getElementById("app");

createInertiaApp({
    resolve: (name) => require(`./Pages/${name}`),
    setup({ el, app, props, plugin }) {
        createApp({ render: () => h(app, props) })
            .mixin({ methods: { route } })
            .use(plugin)
            .use(ElementPlus)
            .use(VueSweetalert2)
            .use(VueGoogleMaps, {
                load: {
                    key: "AIzaSyBsk3p7iIzwFgJwmhrx6Y0p3tl3m2xeFrY",
                },
            })
            .mount(el);
    },
});

InertiaProgress.init({ color: "#4B5563" });
