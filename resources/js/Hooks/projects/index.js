import { reactive, ref } from "vue";

export const _handleTreeData = (treeData, searchValue) => {
    if (!treeData || treeData.length === 0) {
        return [];
    }
    const array = [];
    for (let i = 0; i < treeData.length; i += 1) {
        let match = false;
        for (let pro in treeData[i]) {
            if (typeof treeData[i][pro] == "string") {
                match |= treeData[i][pro].includes(searchValue);
                if (match) break;
            }
        }
        if (
            _handleTreeData(treeData[i].children, searchValue).length > 0 ||
            match
        ) {
            array.push({
                ...treeData[i],
                children: _handleTreeData(treeData[i].children, searchValue),
            });
        }
    }
    return array;
};
export const setExpandRow = (handleTreeData) => {
    const expandRow = ref([]);

    if (handleTreeData.length) {
        for (let i of handleTreeData) {
            expandRow.value.push(i.id);
            if (i.children.length) {
                setExpandRow(i.children);
            }
        }
    }
    return { expandRow };
};
