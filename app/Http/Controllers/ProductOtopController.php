<?php

namespace App\Http\Controllers;

use App\Models\ProductOtop;
use App\Models\Project;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductOtopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ProductOtop::with('project', 'filenames')->paginate();
        return Inertia::render('Admin/Product/index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $projects = Project::has('children', 0)->get();
        return Inertia::render('Admin/Product/form', ['projects' => $projects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'filenames' => 'required|array',
            'filenames.*' => 'required',
            'main_filename' => 'required|array',
            'main_filename.*' => 'required',
            'name' => 'required',
            'project_id' => 'required|integer',
            'detail' => 'required',
        ]);
        $data = $request->all();
        $data['main_filename'] = $request->main_filename[0]['name'];
        $product = ProductOtop::create($data);
        foreach ($request->filenames as $filename) {
            $product->filenames()->create(['filename' => $filename['name']]);
        }
        return redirect(route('admin.products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductOtop  $product
     * @return \Illuminate\Http\Response
     */
    public function show(ProductOtop $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductOtop  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductOtop $product)
    {
        $projects = Project::has('children', 0)->get();
        $product->load([
            'filenames',
            'project'
        ]);
        return Inertia::render('Admin/Product/form', ['projects' => $projects, 'product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductOtop  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductOtop $product)
    {
        $request->validate([
            'filenames' => 'required|array',
            'filenames.*' => 'required',
            'main_filename' => 'required',
            'name' => 'required',
            'project_id' => 'required|integer',
            'detail' => 'required',
        ]);
        $data = $request->all();
        $data['main_filename'] = $request->main_filename[0]['name'];
        $product->update($data);
        $product->filenames()->delete();
        foreach ($request->filenames as $filename) {
            $product->filenames()->create(['filename' => $filename['name']]);
        }
        return redirect(route('admin.products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductOtop  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductOtop $product)
    {
        $product->delete();
        return back();
    }
}
