<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\BudgetYear;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $budget_years = BudgetYear::has('projects')->get();
        $areas = Area::with(
            'projects',
            'projects.stargic',
            'projects.stargic.budgetYear',
            'projects.budgets',
            'projects.projectFiles',
            'projects.objectives',
            'projects.owners',
            'projects.areas',
        )->has('projects')
            ->get();
        $years = BudgetYear::find(1);
        $stargics = $years ? $years->stargics()
            ->has('children')
            ->get() : [];
        $selectStargic = $stargics;
        if (!empty($request->budgetYearId)) {
            $selectStargic = BudgetYear::findOrFail($request->budgetYearId)->stargics;
            $areas = Area::with(
                'projects',
                'projects.budgets',
                'projects.projectFiles',
                'projects.objectives',
                'projects.owners',
                'projects.areas',
            )->has('projects')
                ->whereHas('projects', function ($q)  use ($request) {
                    $q
                        ->where('budget_year_id', $request->budgetYearId);
                })
                ->get();
            if ($request->stargicId) {
                $areas = Area::with(
                    'projects',
                    'projects.budgets',
                    'projects.projectFiles',
                    'projects.objectives',
                    'projects.owners',
                    'projects.areas',
                )
                    ->has('projects')
                    ->whereHas('projects', function ($q) use ($request) {
                        $q
                            ->where('budget_year_id', $request->budgetYearId)
                            ->where('stargic_id', $request->stargicId);
                    })
                    ->get();
            }
        }
        return Inertia::render('ProjectMap', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'areas' => $areas,
            'budget_years' => $budget_years,
            'selectStargic' => $selectStargic
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
