<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    //
    public function store(Request $request)
    {
        if ($request->hasFile('file') and $request->file('file')->isValid()) {
            // $allow = array('application/pdf', 'application/msword', 'xlsx');
            // $mine = $request->file('file')->getMimeType();
            // if (!in_array($mine, $allow)) {
            //     return response()->json(['status' => 0, 'msg' => 'กรุณาเลือกประเภทไฟล์ให้ถูกต้อง']);
            // }

            $max_size = 1024 * 1024 * 3;
            $size = $request->file('file')->getSize();
            if ($size > $max_size) {
                return response()->json(['status' => 0, 'msg' => 'ขนาดไฟล์ต้องไม่เกิน 3M']);
            }

            //original image, upload to local
            $path = $request->file->store('public/uploads');

            //Absolute path
            $file_path = storage_path('app/') . $path;

            // Qiniu_upload($file_path);

            return ['status' => 1, 'file_name' => basename($file_path), 'file_url' => $file_path];
        }
    }
}
