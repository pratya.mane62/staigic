<?php

namespace App\Http\Controllers;

use App\Models\BudgetYear;
use App\Models\Project;
use App\Models\Stargic;
use Illuminate\Http\Request;
use Inertia\Inertia;

class StargicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $budget_years = BudgetYear::with('stargics')
            ->get();
        $stargics = Stargic::get();
        $main_projects = Project::where('parent_id', null)->where('level', 1)->get();


        return Inertia::render(
            'Admin/Stargic/index',
            [
                'budget_years' => $budget_years,
                'stargics' => $stargics,
                'main_projects' => $main_projects
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->stargic_id || $request->budget_year_id) {
            $request->validate([
                'budget_year_id' => 'required|integer',
                'stargic_id' => 'required|integer',
            ]);
            $stargic = Stargic::find($request->stargic_id);
            if ($stargic) {
                $year = $stargic->budgetYear()->find($request->budget_year_id);
                if ($year) {
                    return back()->withErrors([
                        'budget_year_id' => 'ข้อมูลซ้ำ !!',
                        'stargic_id' => 'ข้อมูลซ้ำ !!',
                    ]);
                }
            }
            $stargic->budgetYear()->attach($request->budget_year_id);
        } else {
            $request->validate([
                'number' => 'required|unique:stargics',
                'name' => 'required|unique:stargics',
            ]);
            $data = $request->all();
            Stargic::create($data);
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stargic  $stargic
     * @return \Illuminate\Http\Response
     */
    public function show(Stargic $stargic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stargic  $stargic
     * @return \Illuminate\Http\Response
     */
    public function edit(Stargic $stargic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stargic  $stargic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stargic $stargic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stargic  $stargic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stargic $stargic)
    {
        //
    }
}
