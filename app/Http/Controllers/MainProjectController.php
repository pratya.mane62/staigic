<?php

namespace App\Http\Controllers;

use App\Models\MainProject;
use Illuminate\Http\Request;

class MainProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:main_projects'
        ]);
        $data = $request->all();
        MainProject::create($data);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MainProject  $mainProject
     * @return \Illuminate\Http\Response
     */
    public function show(MainProject $mainProject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MainProject  $mainProject
     * @return \Illuminate\Http\Response
     */
    public function edit(MainProject $mainProject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MainProject  $mainProject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainProject $mainProject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MainProject  $mainProject
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainProject $mainProject)
    {
        //
    }
}
