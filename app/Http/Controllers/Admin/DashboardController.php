<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BudgetYear;
use App\Models\Project;
use App\Models\Stargic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $budget_years = BudgetYear::has('projects')->get();
        $years = BudgetYear::find(1);
        $stargics = $years ? $years->stargics()->with(
            [
                'children' => function ($q) use ($request) {
                    $q->where('budget_year_id', 1);
                },
                'children.stargic',
                'children.budgetYear',
                'children.budgets',
                'children.projectFiles',
                'children.objectives',
                'children.owners',
                'children.areas',
                'children.parent',
                'children.parent.parent',
                'children.parent.parent.parent',
                'children.children',
                'children.children.stargic',
                'children.children.budgetYear',
                'children.children.budgets',
                'children.children.projectFiles',
                'children.children.objectives',
                'children.children.owners',
                'children.children.areas',
                'children.children.parent',
                'children.children.children',
                'children.children.children.stargic',
                'children.children.children.budgetYear',
                'children.children.children.budgets',
                'children.children.children.projectFiles',
                'children.children.children.objectives',
                'children.children.children.owners',
                'children.children.children.areas',
                'children.children.children.parent',
            ]
        )
            ->has('children')
            ->get() : array();
        $chart = Project::select('stargic_id', 'stargics.name', DB::raw('sum(budgets.number) as total'))
            ->join('budgets', 'projects.id', 'budgets.project_id')
            ->join('stargics', 'projects.stargic_id', 'stargics.id')
            ->groupBy('stargic_id')
            ->where('budget_year_id', 1)
            ->get();
        if (!empty($request->budgetYearId)) {
            $stargics = BudgetYear::findOrFail($request->budgetYearId)->stargics()->with(
                [
                    'children' => function ($q) use ($request) {
                        $q->where('budget_year_id', $request->budgetYearId);
                    },
                    'children.stargic',
                    'children.budgetYear',
                    'children.budgets',
                    'children.projectFiles',
                    'children.objectives',
                    'children.owners',
                    'children.areas',
                    'children.parent',
                    'children.parent.parent',
                    'children.parent.parent.parent',
                    'children.children',
                    'children.children.stargic',
                    'children.children.budgetYear',
                    'children.children.budgets',
                    'children.children.projectFiles',
                    'children.children.objectives',
                    'children.children.owners',
                    'children.children.areas',
                    'children.children.parent',
                    'children.children.children',
                    'children.children.children.stargic',
                    'children.children.children.budgetYear',
                    'children.children.children.budgets',
                    'children.children.children.projectFiles',
                    'children.children.children.objectives',
                    'children.children.children.owners',
                    'children.children.children.areas',
                    'children.children.children.parent',
                ]
            )->has('children')
                ->get();
            $chart = Project::select('stargic_id', 'stargics.name', DB::raw('sum(budgets.number) as total'))
                ->join('budgets', 'projects.id', 'budgets.project_id')
                ->join('stargics', 'projects.stargic_id', 'stargics.id')
                ->groupBy('stargic_id')
                ->where('budget_year_id', $request->budgetYearId)
                ->get();
        }
        return Inertia::render('Admin/Dashboard', [
            'stargics' => $stargics,
            'budget_years' => $budget_years,
            'chart' => $chart
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
