<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Budget;
use App\Models\BudgetYear;
use App\Models\Objective;
use App\Models\Owner;
use App\Models\Project;
use App\Models\Stargic;
use Illuminate\Http\Request;
use Inertia\Inertia;

use function PHPSTORM_META\type;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $budget_years = BudgetYear::get();
        $year = $request->yearsId ?? 1;
        $term = $request->term ?? '';
        $project = Project::with(
            'stargic',
            'budgetYear',
            'children',
            'children.children',
            'children.children.children',
            'children.parent',
            'children.children.parent',
            'children.children.children.parent',
        )
            ->where('level', 1)
            ->where('type', 'project')
            ->where('budget_year_id', $year)
            ->where('name', 'Like', '%' . $term . '%')
            ->paginate(10);
        return Inertia::render(
            'Admin/Project/index',
            [
                'project' => $project,
                'budget_years' => $budget_years
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $budget_years = BudgetYear::get();
        $stargics = array();
        $areas = Area::get();
        $owners = Owner::get();
        $main_projects = Project::get();
        $objectives = Objective::get();
        $budgets = Budget::get();
        $year = array();
        $stargic = array();
        $mains = array();
        $main = array();
        $sides = array();
        if ($request->yearsId) {
            $year = BudgetYear::findOrFail($request->yearsId);
            $stargics = $year->stargics;
            if (empty($year)) {
                return redirect(route('admin.projects.create'));
            }
            if ($request->stargicId) {
                $stargic = Stargic::find($request->stargicId);
                $mains = $stargic->projects()
                    ->where('budget_year_id', $request->yearsId)
                    ->where('level', 1)
                    ->get();
                if ($request->mainId) {
                    $main = Project::find($request->mainId);
                    $sides = $stargic->projects()
                        ->where('parent_id', $request->mainId)
                        ->where('budget_year_id', $request->yearsId)
                        ->where('level', 2)
                        ->get();
                }
            }
        }
        return Inertia::render('Admin/Project/form', [
            'budget_years' => $budget_years,
            'stargics' => $stargics,
            'areas' => $areas,
            'owners' => $owners,
            'main_projects' => $main_projects,
            'objectives' => $objectives,
            'budgets' => $budgets,
            'year' => $year,
            'stargic' => $stargic,
            'main' => $main,
            'mains' => $mains,
            'sides' => $sides
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'stargic_id' => 'required|integer',
            'budget_year_id' => 'required|integer',
            'name' => 'required|max:255',
        ]);
        $data = $request->all();
        $data['type'] = 'project';
        $data['project_start'] = $request->project_start ? date('Y-m-d H:i:s', strtotime($request->project_start)) : null;
        $data['project_end'] = $request->project_end ? date('Y-m-d H:i:s', strtotime($request->project_end)) : null;
        if ($request->main_project_id) {
            $data['level'] = 2;
            $data['parent_id'] = $request->main_project_id;
            if ($request->side_id) {
                $data['level'] = 3;
                $data['parent_id'] = $request->side_id;
            }
        }
        $project = Project::create($data);

        if ($request->projectFiles && count($request->projectFiles) > 0) {
            foreach ($request->projectFiles as $filename) {
                $project->projectFiles()->create([
                    'filename' => $filename['name']
                ]);
            }
        }
        if (!empty($request->budgets[0]['number']) && !empty($request->budgets[0]['detail'])) {
            foreach ($request->budgets as $budget) {
                $project->budgets()->create([
                    'number' => $budget['number'],
                    'detail' => $budget['detail']
                ]);
            }
        }
        if (!empty($request->objectives[0]['detail'])) {
            foreach ($request->objectives as $objective) {
                $project->objectives()->create(['detail' => $objective['detail']]);
            }
        }
        if ($request->owners && count($request->owners) > 0) {
            $project->owners()->attach($request->owners);
        }
        if ($request->areas && count($request->areas) > 0) {
            $project->areas()->attach($request->areas);
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, Request $request)
    {
        $budget_years = BudgetYear::get();
        $areas = Area::get();
        $owners = Owner::get();
        $main_projects = Project::get();
        $objectives = Objective::get();
        $budgets = Budget::get();

        $project->load([
            'stargic',
            'budgetYear',
            'budgets',
            'projectFiles',
            'objectives',
            'owners',
            'areas',
        ]);
        $stargics = array();
        $year = array();
        $stargic = array();
        $mains = array();
        $main = array();
        $sides = array();
        if ($request->yearsId) {
            $year = BudgetYear::findOrFail($request->yearsId);
            $stargics = $year->stargics;
            if (empty($year)) {
                return redirect(route('admin.projects.create'));
            }
            if ($request->stargicId) {
                $stargic = Stargic::find($request->stargicId);
                $mains = $stargic->projects()
                    ->where('budget_year_id', $request->yearsId)
                    ->where('level', 1)
                    ->get();
                if ($request->mainId) {
                    $main = Project::find($request->mainId);
                    $sides = $stargic->projects()
                        ->where('parent_id', $request->mainId)
                        ->where('budget_year_id', $request->yearsId)
                        ->where('level', 2)
                        ->get();
                }
            }
        }
        return Inertia::render('Admin/Project/form', [
            'budget_years' => $budget_years,
            'stargics' => $stargics,
            'areas' => $areas,
            'owners' => $owners,
            'main_projects' => $main_projects,
            'objectives' => $objectives,
            'budgets' => $budgets,
            'year' => $year,
            'stargic' => $stargic,
            'main' => $main,
            'mains' => $mains,
            'sides' => $sides,
            'project' => $project
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'stargic_id' => 'required|integer',
            'budget_year_id' => 'required|integer',
            'name' => 'required|max:255',
        ]);
        $data = $request->all();
        $data['type'] = 'project';
        $data['project_start'] = $request->project_start ? date('Y-m-d H:i:s', strtotime($request->project_start)) : null;
        $data['project_end'] = $request->project_end ? date('Y-m-d H:i:s', strtotime($request->project_end)) : null;

        if ($request->main_project_id) {
            $data['level'] = 2;
            $data['parent_id'] = $request->main_project_id;
            if ($request->side_id) {
                $data['level'] = 3;
                $data['parent_id'] = $request->side_id;
            }
        }
        $project->update($data);

        if (count($request->projectFiles) > 0) {
            $project->projectFiles()->delete();
            foreach ($request->projectFiles as $filename) {
                $project->projectFiles()->create([
                    'filename' => $filename['name']
                ]);
            }
        }
        if (count($request->budgets) > 0) {
            $project->budgets()->delete();
            foreach ($request->budgets as $budget) {
                $project->budgets()->create([
                    'number' => $budget['number'],
                    'detail' => $budget['detail']
                ]);
            }
        }
        if (count($request->objectives) > 0) {
            $project->objectives()->delete();
            foreach ($request->objectives as $objective) {
                $project->objectives()->create(['detail' => $objective['detail']]);
            }
        }
        if (count($request->owners) > 0) {
            $project->owners()->detach();
            $project->owners()->attach($request->owners);
        }
        if (count($request->areas) > 0) {
            $project->areas()->detach();
            $project->areas()->attach($request->areas);
        }
        return redirect(route('admin.projects.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect(route('admin.projects.index'));
    }
}
