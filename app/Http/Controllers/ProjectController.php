<?php

namespace App\Http\Controllers;

use App\Models\BudgetYear;
use App\Models\Project;
use App\Models\Stargic;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $budget_years = BudgetYear::has('projects')
            ->get();
        $years = BudgetYear::find(1);
        $stargics = $years ? $years->stargics()->with(
            [
                'children' => function ($q) {
                    $q->where('budget_year_id', 1);
                },
                'children.stargic',
                'children.budgetYear',
                'children.budgets',
                'children.projectFiles',
                'children.objectives',
                'children.owners',
                'children.areas',
                'children.parent',
                'children.parent.parent',
                'children.parent.parent.parent',
                'children.children',
                'children.children.stargic',
                'children.children.budgetYear',
                'children.children.budgets',
                'children.children.projectFiles',
                'children.children.objectives',
                'children.children.owners',
                'children.children.areas',
                'children.children.parent',
                'children.children.children',
                'children.children.children.stargic',
                'children.children.children.budgetYear',
                'children.children.children.budgets',
                'children.children.children.projectFiles',
                'children.children.children.objectives',
                'children.children.children.owners',
                'children.children.children.areas',
                'children.children.children.parent',
            ]
        )
            ->has('children')
            ->get() : [];
        $selectStargic = $stargics;
        if (!empty($request->budgetYearId)) {
            $years = BudgetYear::findOrFail($request->budgetYearId);
            $stargics = $years->stargics()->with(
                [
                    'children' => function ($q) use ($request) {
                        $q->where('budget_year_id', $request->budgetYearId);
                    },
                    'children.stargic',
                    'children.budgetYear',
                    'children.budgets',
                    'children.projectFiles',
                    'children.objectives',
                    'children.owners',
                    'children.areas',
                    'children.parent',
                    'children.parent.parent',
                    'children.parent.parent.parent',
                    'children.children',
                    'children.children.stargic',
                    'children.children.budgetYear',
                    'children.children.budgets',
                    'children.children.projectFiles',
                    'children.children.objectives',
                    'children.children.owners',
                    'children.children.areas',
                    'children.children.parent',
                    'children.children.children',
                    'children.children.children.stargic',
                    'children.children.children.budgetYear',
                    'children.children.children.budgets',
                    'children.children.children.projectFiles',
                    'children.children.children.objectives',
                    'children.children.children.owners',
                    'children.children.children.areas',
                    'children.children.children.parent',
                ]
            )->has('children')
                ->get();
            $selectStargic = $stargics;
            if ($request->stargicId) {
                $stargics =  $years->stargics()->with(
                    [
                        'children' => function ($q) use ($request) {
                            $q->where('budget_year_id', $request->budgetYearId);
                        },
                        'children.stargic',
                        'children.budgetYear',
                        'children.budgets',
                        'children.projectFiles',
                        'children.objectives',
                        'children.owners',
                        'children.areas',
                        'children.parent',
                        'children.parent.parent',
                        'children.parent.parent.parent',
                        'children.children',
                        'children.children.stargic',
                        'children.children.budgetYear',
                        'children.children.budgets',
                        'children.children.projectFiles',
                        'children.children.objectives',
                        'children.children.owners',
                        'children.children.areas',
                        'children.children.parent',
                        'children.children.children',
                        'children.children.children.stargic',
                        'children.children.children.budgetYear',
                        'children.children.children.budgets',
                        'children.children.children.projectFiles',
                        'children.children.children.objectives',
                        'children.children.children.owners',
                        'children.children.children.areas',
                        'children.children.children.parent',
                    ]
                )
                    ->has('children')
                    ->where('stargics.id', $request->stargicId)
                    ->get();
            }
        }
        return Inertia::render('Welcome', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
            'stargics' => $stargics,
            'budget_years' => $budget_years,
            'selectStargic' => $selectStargic
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
