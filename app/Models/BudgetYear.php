<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BudgetYear extends Model
{
    use HasFactory;
    protected $fillable = ['year'];

    public function stargics()
    {
        return $this->belongsToMany(Stargic::class);
    }
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
