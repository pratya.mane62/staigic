<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stargic extends Model
{
    use HasFactory;
    protected $fillable = [
        'number',
        'name',
    ];
    public function budgetYear()
    {
        return $this->belongsToMany(BudgetYear::class);
    }
    public function children()
    {
        return $this->hasMany(Project::class)->where('level', 1);
    }
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
