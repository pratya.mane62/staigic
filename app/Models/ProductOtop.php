<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductOtop extends Model
{
    use HasFactory;
    protected $fillable = [
        'main_filename',
        'name',
        'detail',
        'project_id'
    ];
    public function filenames()
    {
        return $this->hasMany(FileProductOtop::class);
    }
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
