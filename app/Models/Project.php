<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'project_start',
        'project_end',
        'link',
        'stargic_id',
        'budget_year_id',
        'level',
        'parent_id',
        'type'
    ];
    public function parent()
    {
        return $this->belongsTo(Project::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Project::class, 'parent_id');
    }
    public function stargic()
    {
        return $this->belongsTo(Stargic::class);
    }
    public function budgetYear()
    {
        return $this->belongsTo(BudgetYear::class);
    }
    public function budgets()
    {
        return $this->hasMany(Budget::class);
    }
    public function projectFiles()
    {
        return $this->hasMany(FileProject::class);
    }
    public function objectives()
    {
        return $this->hasMany(Objective::class);
    }
    public function owners()
    {
        return  $this->belongsToMany(Owner::class);
    }
    public function areas()
    {
        return  $this->belongsToMany(Area::class);
    }
}
