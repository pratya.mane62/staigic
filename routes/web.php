<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProjectController as AdminProjectController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\BudgetYearController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\ProductOtopController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\StargicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// })->name('welcome');

// Route::get('/', [ProjectController::class, 'index'])->name('index');
Route::resources([
    '/' => ProjectController::class,
    '/map' => MapController::class,
]);

Route::post('/upload', [FileUploadController::class, 'store'])->name('upload');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'verified']], function () {
    Route::resource('/dashboard', DashboardController::class);
    Route::resource('/projects', AdminProjectController::class);
    Route::resource('/budget/years', BudgetYearController::class);
    Route::resource('/stargics', StargicController::class);
    Route::resource('/owners', OwnerController::class);
    Route::resource('/areas', AreaController::class);
    Route::resource('/products', ProductOtopController::class);
});
require __DIR__ . '/auth.php';
